#include "StringTools.h"

using namespace std;

vector<string> StringTools::splitString(const string &input, string delimiter, bool stripParamBrackets)
{
	vector<string> result;
	string token;
	int pos = 0;
	string copyOfInput(input);
	
	if(stripParamBrackets)
		copyOfInput = stripParametersBrackets(copyOfInput);

	while( (pos = copyOfInput.find(delimiter)) != string::npos)
	{
		token = copyOfInput.substr(0, pos);

		result.push_back(token);
		copyOfInput.erase(0, pos + delimiter.length());
	}

	result.push_back(copyOfInput);

	return result;
}

string StringTools::capitalizeOnlyFirstLetter(const string &input)
{
	int size = input.length();
	string output(input);

	for(int i = 1 ; i < size ; ++i)
		output.at(i) += 0x20;

	return output;
}

string StringTools::stripParametersBrackets(const string &input)
{
	string out(input);
	int pos = 0;

	pos = out.find("[");
	out.erase(pos,1);
	pos = out.find("]");
	out.erase(pos,1);

	return out;
}

double StringTools::convertStringToDouble(const string &input)
{
	return ::atof(input.c_str());
}

string StringTools::stripEndOfLineComment(const string &input)
{
	std::string output(input);
	int pos = -1;

	pos = output.find_last_of("]") + 1;
	if(pos != string::npos)
		output.erase(pos, output.length());

	return output;
}