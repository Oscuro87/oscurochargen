#include <iostream>
#include <ctime>

#include "main.h"

using namespace std;

int main(void)
{
	srand(time(NULL));

	Actor actor("human");
	actor.describe();

	cout << endl << endl << "Press a key to quit..." << endl;
	getchar();
	
	return 0;
}