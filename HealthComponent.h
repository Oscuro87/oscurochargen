#pragma once

struct HealthComponent
{
	int maxHealth;
	int health;
	int variation;

	HealthComponent() : maxHealth(0), health(0), variation(0)
	{}

	HealthComponent(HealthComponent const& other) : maxHealth(other.maxHealth), health(other.health), variation(other.variation)
	{}
};