#include "RaceFactory.h"
#include <algorithm>

using namespace std;

RaceFactory::RaceFactory()
{

}

RaceFactory::~RaceFactory()
{
	if(currentRace != 0)
		delete currentRace;
}

void RaceFactory::loadRaceInfo(string raceName)
{
	ifstream in;
	string line;
	vector<string> split;

	if(currentRace != 0)
		delete currentRace;
	currentRace = new Race();
	loadedFlag = false;
	ignoreFlag = false;

	transform(raceName.begin(), raceName.end(), raceName.begin(), ::toupper);
	raceNameToLoad = raceName;

	in.open("data/races.dat");

	if(!in.is_open())
	{
		cout << "Unable to open the races.dat file! Exiting..." << endl;
		exit(-2);
	}
	else
	{
		while(getline(in, line))
		{
			if(loadedFlag) break;
			
			split.clear();

			if(line[0] == '-' && line[1] == '-') continue; // Ignore comments
			if(line == "") continue; // Ignore blank lines
			
			line = StringTools::stripEndOfLineComment(line); // Remove possible comments after parameters

			// Split multiple parameters separated with whitespace
			split = StringTools::splitString(line, " ", false);
			line.clear();
			
			// Process them individually
			for(vector<string>::iterator it = split.begin() ; it != split.end() ; ++it)
				processParameter(*it);
		}
	}

	in.close();

	if(currentRace->raceName == "")
		cout << "Couldn't find the race named: " << raceName << endl;
}

void RaceFactory::processParameter(string param)
{
	vector<string> split = StringTools::splitString(param, ":", true);

	// RACE HANDLING
	if(split.at(0) == "RACE")
	{
		if(split.at(2) == "START")
		{
			if(raceNameToLoad != split.at(1)) // Avoid loading the wrong race
			{
				ignoreFlag = true;
				return;
			}
			else
			{
				currentRace->raceName = StringTools::capitalizeOnlyFirstLetter(split.at(1));
			}
		}
		else
		{
			if(!ignoreFlag) loadedFlag = true;
			if(!loadedFlag) ignoreFlag = false;
			return;
		}
	}
	else if(ignoreFlag)
		return;
	// STARTING HEALTH
	else if(split.at(0) == "STARTINGHEALTH")
	{
		string health = split.at(1);
		double result = StringTools::convertStringToDouble(health);
		currentRace->healthComponent->maxHealth = (int)result;
		currentRace->healthComponent->health = (int)result;
	}
	// HEALTH VARIATION
	else if(split.at(0) == "VARIATION" && !ignoreFlag)
	{
		string variation = split.at(1);
		double result = StringTools::convertStringToDouble(variation);
		currentRace->healthComponent->variation = (int)result;
	}

	// NOT RECOGNIZED TAGS HERE
	else
	{
		if(DEBUGMODE)
		{
			cout << "Encountered an unrecognized tag, ignoring. (Data is: ";
			for(vector<string>::iterator it = split.begin(); it != split.end() ; ++it)
				cout << *it << " ";
			cout << ")" << endl;
		}
	}
}