#include "Actor.h"
#include "NameGenerator.h"

using namespace std;

Actor::Actor(string raceName)
{
	generateName();
	setupRace(raceName);
}

Actor::~Actor()
{
	name.clear();
	if(actorRace != 0)
		delete actorRace;
}

void Actor::generateName()
{
	NameGenerator ng;
	int genSize = 32;
	
	do
	{
		name = ng.getGeneratedName(genSize, FIRSTNAME_AND_SURNAME);
	} while(name == "");
}

void Actor::setupRace(string raceName)
{
	Race* r;
	RaceFactory::getInstance().loadRaceInfo(raceName);
	r = RaceFactory::getInstance().getLoadedRace();

	if(r == 0)
		exit(-1);

	this->actorRace = new Race(*r);
}

void Actor::describe()
{
	cout << name << " is a " << actorRace->raceName << endl;
	cout << "His health is currently: " << actorRace->healthComponent->health << "/" << actorRace->healthComponent->maxHealth << endl;
}