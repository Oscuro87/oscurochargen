#pragma once

#include "main.h"


#define NAME_MAX_SIZE_DEFAULT 32

enum NameGenerationType
{
	FIRSTNAMEONLY,
	FIRSTNAME_AND_SURNAME,
};

class NameGenerator
{
private:
	unsigned int nameMaxSize;
	int mediumStart, longStart;
	std::string currentName;
	std::vector<std::string> names;
	NameGenerationType currentGenerationType;

	void init();
	int getNameSize();
	void generateName();

public:
	NameGenerator();
	~NameGenerator();

	std::string getGeneratedName(int nameMaxSize, NameGenerationType type);
};