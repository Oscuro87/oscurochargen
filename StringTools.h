#pragma once

#include "main.h"

class StringTools
{
private:
	StringTools() {}
	StringTools(StringTools const&);
	void operator=(StringTools const&);

public:
	static std::vector<std::string> splitString(const std::string &input, std::string delimiter, bool stripParamBrackets);
	static std::string capitalizeOnlyFirstLetter(const std::string &input);
	static std::string stripParametersBrackets(const std::string &input);
	static double convertStringToDouble(const std::string &input);
	static std::string stripEndOfLineComment(const std::string &input);
};