#pragma once

#include "main.h"

struct Race
{
	std::string raceName;
	HealthComponent* healthComponent;

	Race() : raceName(""), healthComponent(new HealthComponent())
	{}
	
	Race(Race const& other) : raceName(other.raceName), healthComponent(new HealthComponent( *(other.healthComponent) ))
	{}
};