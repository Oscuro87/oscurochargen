#pragma once

#include <ctime>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <map>

#define TRUE 1
#define FALSE 0
#define DEBUGMODE FALSE

#include "StringTools.h"

#include "HealthComponent.h"
#include "Race.h"
#include "Actor.h"
#include "NameGenerator.h"
#include "RaceFactory.h"