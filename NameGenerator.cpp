#include "NameGenerator.h"

#define MAX_GENERATION_ATTEMPTS 10

using namespace std;

NameGenerator::NameGenerator() : nameMaxSize(NAME_MAX_SIZE_DEFAULT), currentGenerationType(FIRSTNAMEONLY)
{
	init();
}

NameGenerator::~NameGenerator()
{
	names.clear();
}

void NameGenerator::init()
{
	string line;
	ifstream in;
	in.open("data/names.dat");
	int entryCounter = 0;

	if(in.is_open())
	{
		while(getline(in, line))
		{
			if(line == "")
				continue;
			else if(line.at(0) == '[' && line.at(line.length()-1) == ']') // If it's a beacon...
			{
				std::string beaconName = line.substr(1, line.length()-2); // -2 because there's the EOL character! (\n)
				//cout << "Fount beacon named : " << beaconName << endl;
				
				if(beaconName == "MEDIUM")
				{
					//cout << "Found beacon [MEDIUM] starting at occurence: " << entryCounter << endl;
					mediumStart = entryCounter;
				}
				else if(beaconName == "LONG")
				{
					//cout << "Found beacon [LONG] starting at occurence: " << entryCounter << endl;
					longStart = entryCounter;
				}
				/*
				else if(beaconName == "SHORT")
					cout << "Found beacon [SHORT]" << endl;
				else
					cout << "Unknown beacon..." << endl;
				*/
			}
			else
			{
				++entryCounter;
				names.push_back(line);
			}
		}
	}
	else
	{
		cout << "Unable to open the names.dat file! Exiting...";
		getchar();
		exit(0);
	}

	in.close();
}

int NameGenerator::getNameSize()
{
	return currentName.size();
}

void NameGenerator::generateName()
{
	std::string generated;
	int firstNameRand, surnameRand;

	switch(currentGenerationType)
	{
	default:
	case FIRSTNAMEONLY:
		firstNameRand = rand() % names.size();
		generated = names.at(firstNameRand);
		break;

	case FIRSTNAME_AND_SURNAME:
		firstNameRand = rand() % names.size();
		surnameRand = rand() % names.size();
		generated = names.at(firstNameRand) + " " + names.at(surnameRand);
		break;
	}

	currentName = generated;
}

string NameGenerator::getGeneratedName(int nameMaxSize, NameGenerationType type)
{
	this->nameMaxSize = nameMaxSize;
	this->currentGenerationType = type;
	int attempts = 0;

	do
	{
		++attempts;
		if(attempts > MAX_GENERATION_ATTEMPTS)
			break;
		generateName();
		if(currentName.size() > this->nameMaxSize)
			currentName.clear();
	} while(currentName == "");

	return currentName;
}