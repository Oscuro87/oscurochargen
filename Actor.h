#pragma once

#include "main.h"

class Actor
{
private:
	void generateName();
	void setupRace(std::string raceName);

public:
	std::string name;
	Race* actorRace;

	Actor(std::string raceName);
	~Actor();

	void describe();
};