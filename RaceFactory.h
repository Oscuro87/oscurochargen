#pragma once

#include "main.h"

class RaceFactory
{
private:
	RaceFactory();

	RaceFactory(RaceFactory const&);
	void operator=(RaceFactory const&);

	std::string raceNameToLoad;
	Race* currentRace;
	
	bool ignoreFlag;
	bool loadedFlag;

	void processParameter(std::string param);

public:
	~RaceFactory();
	
	inline static RaceFactory& getInstance()
	{
		static RaceFactory inst;
		return inst;
	}

	void loadRaceInfo(std::string raceName);
	
	inline Race* getLoadedRace()
	{ 
		if(currentRace != 0)
		{
			Race* copy = new Race(*currentRace);
			return copy;
		}
		else return 0;
	}
};